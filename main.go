package main

import "fmt"


func main() {
	var numero int
	fmt.Println("escribe un número: ")
	fmt.Scanf("%d", &numero)

	Impar(numero)
	Primo(numero)
}

func Impar(number int)  {
	if number % 2 != 0 {
		fmt.Println("El número es impar")
	} else {
		fmt.Println("El número no es impar")
	}
}

func Primo(number int)  {
	iteracion := 1
	divisionResiduo := 0

	for iteracion <= number {
		if number % iteracion == 0 {
			divisionResiduo += 1
		}

		iteracion += 1
	}

	if divisionResiduo == 2 {
		fmt.Println("El número es primo")
	} else {
		fmt.Println("El número no es primo")
	}
}